#!/bin/python3

import collections as col
import itertools as it
import sys

LOWER  = 0
HIGHER = 1

def create_dice_rolls(dice_sides, dice_count, take, higher_lower):
    rolls = it.product(range(1, dice_sides+1), repeat=dice_count)
    if higher_lower == HIGHER:
        f = lambda r: sorted(r)[-take:]
    else:
        f = lambda r: sorted(r)[:take]
    return map(lambda r: f(r), rolls)

def sum_dice_rolls(rolls, mod):
    return sorted(map(lambda r: sum(r) + mod, rolls))

def tally_results(sums):
    return col.Counter(sums)

def print_histogram(hist):
    """A horizontal frequency-table/histogram plot."""
    for k in sorted(hist.items()):
        print('{0:5d} {1}'.format(k[0], '+' * k[1]))

def main():
    dice_sides, dice_count, take, higher_lower, mod = map(lambda a: int(a), sys.argv[1:6])

    rolls = create_dice_rolls(dice_sides, dice_count, take, higher_lower)
    sums  = sum_dice_rolls(rolls, mod)
    tally = tally_results(sums)
    print_histogram(tally)

if __name__ == "__main__":
    main()
